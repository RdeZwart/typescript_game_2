Rocinante 

Bitbucked: 
https://bitbucket.org/RdeZwart/typescript_game_2

Online version: 
http://rocinante.rachelledezwart.nl/

How to play: 
Gebruik de WASD keys om de ongevulde circles te ontwijken. Pak de groen en blauwe boosters. De groene booster geeft 1 extra health en 5 punten. De blauwe booster geeft 10 extra punten. Blijf zo lang mogelijk in leven 


![Rocinante](assets/classDiagram/rocinante.jpg)
